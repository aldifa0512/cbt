<?php

// Menggunakan PHP 5.4 Or Less 
// 

define('APP_NAME', 'SMART CBT'); // Untuk saat ini hanya digunakan untuk nama file backup database
define('APP_VERSION', 'V1.12');
define('NAMA_SEKOLAH', 'SMKN2 PAYAKUMBUH');
define('LOGO', TRUE);
define('SELF_DESTROY', FALSE);
define('SELF_DESTROY_DATE', ''); // YYYY-MM-DD ex: 2017-01-05
define("RESTORE_DB_ENABLE", FALSE);
define("PASS_7Z", 'aldifajrin');

// DATABASE CONSTS----------------------------------------------------------------------
define('HOST', 'localhost');
define('DATABASE', 'k6449440_cbt');
define('DATABASE_USER', 'k6449440_cbt');
define('DATABASE_PASSWORD', 'admin123');

// STATUS UJIAN SISWA CONSTS------------------------------------------------------------
// JANGAN DIGANTI DULU------------------------------------------------------------------
define("BELUM_LOGIN", 0);
define("SEDANG_UJIAN", 1);
define("SELESAI_UJIAN", 9);

define('RUNNING_TEXT', '<span style="color:#4286f4; font-size:15pt;" >Selamat datang di aplikasi ujian berbasis komputer ' . NAMA_SEKOLAH . ', Silahkan login dengan akun yang telah didaftarkan sebelumnya.</span>');

define("ORIGINAL_PROJECT", 'WOKA INC - CBT PROJECT');
define("DEV_BY", "ALDI FAJRIN | ALFA IT SOLUTION | https://www.facebook.com/aldifa0512");


// -------------------------------------------------------------------------------------
// KONSTAN PERHITUNGAN NILAI------------------------------------------------------------
define("PERSEN_OBJEKTIF", 70); // Dalam Puluhan
define("PERSEN_ESSAY", 30); // Dalam Puluhan


// 
// 
// *
// * *
// * * *
// Original App by Woka Inc | CBT Project  
// Modified by Aldi Fajrin | https://www.facebook.com/aldifa0512
// Big thanks to Woka Inc.
// *
// * * *
// * *
// *
// 
// 
// 
// -----------------------------------------------------------------------------------
//
// Major changes detail :
// 1. constant.php untuk setting koneksi dan  dinamis 
// 2. Acak jawaban soal.
// 3. Perbaikan laporan rekap nilai .pdf dan .xls
// 4. Penambahan Flash Message pada admin
// 5. Perbaikan beberapa tampilan.
// 6. Perbaikan beberapa fitur.
// 7. Soal Essay 
// 8. Setting Persentase Nilai Ujian
// 
// -----------------------------------------------------------------------------------
// 
?>
