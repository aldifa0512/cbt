<?php

// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------
// Define styles dan class
// Perhatikan nama msg nya, nama msg akan menentukan tampilan message. hanya menerima  nilai yg terdapata pada $color
// 
    function createStyles($name = 'success'){
        $color = array(
            'error' => '#e81414',
            'success' => '#09bfb0',
        );

        $styles = 'padding:10px;clear:both; width:100%;color:' . $color[$name] . ';';
        return $styles;
    }

    function showFlashMessages(){
        flash( 'success' );
        flash( 'error' );   
    }

/**
 * Function to create and display error and success messages
 * @access public
 * @param string session name
 * @param string message
 * @param string display class
 * @return string message
 */
function flash( $name = '', $message = '')
{
    //We can only do something if the name isn't empty
    if( !empty( $name ) )
    {
        //No message, create it
        if( !empty( $message ) && empty( $_SESSION[$name] ) )
        {
            if( !empty( $_SESSION[$name] ) )
            {
                unset( $_SESSION[$name] );
            }

            $_SESSION[$name] = $message;
        }
        //Message exists, display it
        elseif( !empty( $_SESSION[$name] ) && empty( $message ) )
        {
            $styles = createStyles($name);
            echo '<div style="clear:both;width:100%;"><span style="' . $styles . '"><b>'.$_SESSION[$name].'</b></span></div>';
            unset($_SESSION[$name]);
        }
    }
}


// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------
// Cara menggunakan cukup masukan ke redirect code nya
// 

    //Set the first flash message with default class
    // flash( 'success', 'Berhasil menghapus table Siswa' );

// 
// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------
?>