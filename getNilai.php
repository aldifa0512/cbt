<?php
	function getNilaiAkhir($var_siswa, $var_soal, $var_token, $pa){
		$queryJumlahBenarObjektif = "SELECT s.XEssay, j.XNilai as HasilUjian FROM `cbt_jawaban` j left join cbt_soal s on s.XNomerSoal = j.XNomerSoal WHERE j.XKodeSoal = '$var_soal' and  s.XKodeSoal = '$var_soal' and  j.XUserJawab = '$var_siswa' and j.XTokenUjian = '$var_token' order by j.Urut";
		$sqo = mysql_query($queryJumlahBenarObjektif);

		$jumlahSoalEssay = 0;
		$jumlahBetulEssay = 0;
		$jumlahSoalObjektif = 0;
		$jumlahBetulObjektif = 0;
		while($p = mysql_fetch_array($sqo)) {
			//Menghitung betul Essay
			if($p['XEssay'] == 1){
				$jumlahSoalEssay++;
				if($p['HasilUjian'] == 1){
					$jumlahBetulEssay++;
				}
			}else{
				// yang ini untuk betul objektif
				$jumlahSoalObjektif++;
				if($p['HasilUjian'] == 1){
					$jumlahBetulObjektif++;
				}
			}	
		}

		$persen_objektif = $pa['p_objektif'];
		$persen_essay = $pa['p_essay'];

		$p_essay = intval($persen_essay) * intval($jumlahBetulEssay);
		$p_objektif = intval($persen_objektif) * intval($jumlahBetulObjektif);

		$nilaiEssay = intval($jumlahSoalEssay) != 0 ? $p_essay / intval($jumlahSoalEssay) : 0;
		$nilaiObjektif = intval($jumlahSoalObjektif) != 0 ? $p_objektif / intval($jumlahSoalObjektif) : 0;
		$totalnilai = $nilaiEssay + $nilaiObjektif;

		return $totalnilai;
	}
?>