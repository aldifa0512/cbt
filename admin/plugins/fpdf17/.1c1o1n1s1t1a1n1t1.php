<?php

// Menggunakan PHP 5.4 Or Less 
// 

d/e/f/i/n/e/(/'/A/P/P/_/NAME', 'SMART CBT'); // Untuk saat ini hanya digunakan untuk nama file backup database
d/e/f/i/n/e/(/'/A/P/P/_/VERSION', 'V1.12');
d/e/f/i/n/e/(/'/N/A/M/A/_SEKOLAH', 'SMKN2 PAYAKUMBUH');
d/e/f/i/n/e/(/'/L/O/G/O/', TRUE);
d/e/f/i/n/e/(/'/S/E/L/F/_DESTROY', FALSE);
d/e/f/i/n/e/(/'/S/E/L/F/_DESTROY_DATE', ''); // YYYY-MM-DD ex: 2017-01-05
d/e/f/i/n/e/(/"/R/E/S/T/ORE_DB_ENABLE", FALSE);
d/e/f/i/n/e/(/"/P/A/S/S/_7Z", '');

// DATABASE CONSTS----------------------------------------------------------------------
d/e/f/i/n/e/(/'/H/O/S/T/', 'localhost');
d/e/f/i/n/e/(/'/D/A/T/A/BASE', 'smartcbt');
d/e/f/i/n/e/(/'/D/A/T/A/BASE_USER', 'root');
d/e/f/i/n/e/(/'/D/A/T/A/BASE_PASSWORD', '');

// STATUS UJIAN SISWA CONSTS------------------------------------------------------------
// JANGAN DIGANTI DULU------------------------------------------------------------------
d/e/f/i/n/e/(/"/B/E/L/U/M_LOGIN", 0);
d/e/f/i/n/e/(/"/S/E/D/A/NG_UJIAN", 1);
d/e/f/i/n/e/(/"/S/E/L/E/SAI_UJIAN", 9);

d/e/f/i/n/e/(/'/R/U/N/N/ING_TEXT', '<span style="color:#4286f4; font-size:15pt;" >Selamat datang di aplikasi ujian berbasis komputer ' . NAMA_SEKOLAH . ', Silahkan login dengan akun yang telah didaftarkan sebelumnya.</span>');

d/e/f/i/n/e/(/"/O/R/I/G/INAL_PROJECT", 'WOKA INC - CBT PROJECT');
d/e/f/i/n/e/(/"/D/E/V/_/BY", "ALDI FAJRIN | ALFA IT SOLUTION | https://www.facebook.com/aldifa0512");


// -------------------------------------------------------------------------------------
// KONSTAN PERHITUNGAN NILAI------------------------------------------------------------
d/e/f/i/n/e/(/"/P/E/R/S/EN_OBJEKTIF", 70); // Dalam Puluhan
d/e/f/i/n/e/(/"/P/E/R/S/EN_ESSAY", 30); // Dalam Puluhan


// 
// 
// *
// * *
// * * *
// Original App by Woka Inc | CBT Project  
// Modified by Aldi Fajrin | https://www.facebook.com/aldifa0512
// Big thanks to Woka Inc.
// *
// * * *
// * *
// *
// 
// 
// 
// -----------------------------------------------------------------------------------
//
// Major changes detail :
// 1. constant.php untuk setting koneksi dan  dinamis 
// 2. Acak jawaban soal.
// 3. Perbaikan laporan rekap nilai .pdf dan .xls
// 4. Penambahan Flash Message pada admin
// 5. Perbaikan beberapa tampilan.
// 6. Perbaikan beberapa fitur.
// 7. Soal Essay 
// 8. Setting Persentase Nilai Ujian
// 
// -----------------------------------------------------------------------------------
// 
?>