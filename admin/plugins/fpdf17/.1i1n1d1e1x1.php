<?php

// Handle HEADERS 
ob_start();

// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------
// iINCLUDE VENDOR DAN CONSTANT PADA ROOT DIRECTORY
// 
   
    require_once('../constant.php');
    require_once("../cbt_con.php");
    require_once('../vendor/session.php');
    require_once('../vendor/FlashMessages.php');

    require_once('databasesFunction.php');
    require_once('sd.php');
    

// 
// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------



if(!isset($_SESSION['login'])) {
    include("login.php");
}
else {
?>
<html lang="en">
   <head>
        <meta charset="utf-8">
        <title>ADMIN | CBT APLIKASI</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css">
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css">
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css">
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css">
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/layout6/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="../assets/layouts/layout6/css/custom.min.css" rel="stylesheet" type="text/css">
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="../admin/images/icon.ico"> </head>
    <!-- END HEAD -->

    <body >
    <script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript">
    var htmlobjek;

    $(document).ready(function(){
    //apabila terjadi event onchange terhadap object <select id=propinsi>
    $("#txt_level").change(function(){
    var txt_level = $("#txt_level").val();
    var txt_mapel = $("#txt_mapel").val();
    $.ajax({
    url: "ambil_level.php",
    data: "txt_level="+txt_level+"&txt_mapel="+txt_mapel,
    cache: false,
    success: function(msg){
    //jika data sukses diambil dari server kita tampilkan
    //di <select id=kota>
    $("#txt_ujian").html(msg);
    }
    });
    });



    $("#txt_ujian").change(function(){
    var txt_ujian = $("#txt_ujian").val();
    $.ajax({
    url: "ambil_token.php",
    data: "txt_ujian="+txt_ujian,
    cache: false,
    success: function(msg){
    $("#txt_token").val(msg);
    }
    });
    });
    
    $("#txt_ujian").change(function(){
    var txt_ujian = $("#txt_ujian").val();
    $.ajax({
    url: "ambil_soal.php",
    data: "txt_ujian="+txt_ujian,
    cache: false,
    success: function(msg1){
    $("#txt_jumsoal").html(msg1);
    }
    });
    });

});
 
</script>
        <!-- BEGIN HEADER -->
        <header class="page-header">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <?php if(constant('LOGO')) { ?>
                    <div style="float: left; max-width: 80px; margin: 10px;">
                        <img src="../images/logo.png" style="max-width: 100%;">
                    </div>
                    <?php } ?>
                    <style type="text/css">
                        .app-name{
                            position: relative;
                            margin: 10px;
                            font-size: 20pt;
                            float: left;
                        }
                    </style>
                    <div class="app-name">
                        <span><a href="index.php"><?= constant('APP_NAME') ?></a></span><br>
                        <span><a href="index.php"><?= constant('NAMA_SEKOLAH') ?></a></span>
                    </div>                    
                    <span style="float: right;padding-right: 30px;"><H3><FONT class="font-green" ><strong>ONLINE</strong></FONT></H3></span>
                </div>
                <!--/container-->
            </nav>
        </header>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="container-fluid">
            <div class="page-content page-content-popup">
                <div class="page-content-fixed-header">
                    <!-- BEGIN BREADCRUMBS -->
                    <ul class="page-breadcrumb">
                      <li>Dashboard</li>
                      
                    </ul>
                    <!-- END BREADCRUMBS -->
                    <div class="content-header-menu">
                        <!-- BEGIN DROPDOWN AJAX MENU -->
                        <div class="dropdown-ajax-menu btn-group">
                          <ul class="dropdown-menu-v2">
                          <li>
                                    <a href="start.html">Application</a>
                            </li>
                                <li>
                                    <a href="start.html">Reports</a>
                                </li>
                                <li>
                                    <a href="start.html">Templates</a>
                                </li>
                                <li>
                                    <a href="start.html">Settings</a>
                                </li>
                          </ul>
                        </div>
                        <!-- END DROPDOWN AJAX MENU -->
                        <!-- BEGIN MENU TOGGLER -->
                    <button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </span>
                      </button>
                        <!-- END MENU TOGGLER -->
                    </div>
                </div>
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="nav-item  ">
                                <a href="index.php" class="nav-link ">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                              </a>
                                <a href="index.php?page=status_tes" class="nav-link ">
                                    <i class="icon-note"></i>
                                    <span class="title">Status Tes</span>
                                    </a>
                                <a href="index.php?page=daftar_peserta" class="nav-link ">
                                    <i class="icon-docs"></i>
                                    <span class="title">Daftar Peserta</span>
                                </a>
                               <!--<a href="index.php?page=pengaturan_tes" class="nav-link ">
                                    <i class="icon-cloud-download"></i>
                                    <span class="title">Pengaturan Tes</span>
                              </a>-->
                              <a href="index.php?page=pengaturan_kelas" class="nav-link ">
                                    <i class="icon-cloud-download"></i>
                                    <span class="title">Daftar Kelas</span>
                              </a>
                              <a href="index.php?page=pengaturan_mapel" class="nav-link ">
                                    <i class="icon-cloud-download"></i>
                                    <span class="title">Daftar Mapel</span>
                              </a>
                                 <a href="index.php?page=status_peserta" class="nav-link ">
                                    <i class="icon-user"></i>
                                    <span class="title">Status Peserta</span>
                              </a>
                                <a href="index.php?page=reset" class="nav-link ">
                                    <i class="icon-refresh"></i>
                                    <span class="title">Reset Login Peserta</span>
                                </a>
                                <a href="index.php?page=buat_soal" class="nav-link ">
                                    <i class="icon-pencil"></i>
                                    <span class="title">Buat Soal</span>
                                </a>
                                <a href="index.php?page=hasil_tes" class="nav-link ">
                                    <i class="icon-book-open"></i>
                                    <span class="title">Laporan Tes</span>
                                </a>
                                <a href="index.php?page=backup_restore" class="nav-link">
                                    <i class="icon-folder-alt"></i>
                                    <span class="title">Backup & Restore </span>
                                    </a>
                                <a href="logout.php" class="nav-link ">
                                    <i class="icon-logout"></i>
                                    <span class="title">Log Out</span>
                                </a>
                            </li>
                            
                            </li>
                        </ul>
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
              <div class="page-fixed-main-content">
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <?php   
        if(!isset($_GET["page"])){
            require "content.php"; 
        }else{
            if($_GET['page'] == "status_tes"){
                require "status_tes.php";
            }else if($_GET['page'] == "content"){
                require "";
            }else if($_GET['page'] == "daftar_peserta"){
                require "daftar_peserta.php";
            }else if($_GET['page'] == "edit_peserta"){
                require "edit_peserta2.php";
            }else if($_GET['page'] == "status_peserta"){
                require "status_peserta.php";
            }else if($_GET['page'] == "tsiswa"){
                require "tsiswa.php";
            }else if($_GET['page'] == "tkelas"){
                require "tkelas.php";
            }else if($_GET['page'] == "tmapel"){
                require "tmapel.php";
            }else if($_GET['page'] == "pengaturan_tes"){
                require "pengaturan_tes.php";
            }else if($_GET['page'] == "pengaturan_kelas"){
                require "pengaturan_kelas.php";
            }else if($_GET['page'] == "edit_kelas"){
                require "edit_kelas2.php";      
            }else if($_GET['page'] == "pengaturan_mapel"){
                require "pengaturan_mapel.php";     
            }else if($_GET['page'] == "edit_mapel"){
                require "edit_mapel2.php";      
            }else if($_GET['page'] == "reset"){
                require "tampiltabel.php";  
            }else if($_GET['page'] == "hasil_tes"){
                require "hasil_tes.php";
            }else if($_GET['page'] == "buat_soal"){
                require "backup.php";   
            }else if($_GET['page'] == "list_soal"){
                require "list_soal2.php";   
            }else if($_GET['page'] == "ubah_soal"){
                require "ubah_soal2.php";   
            }elseif($_GET['page'] == "backup_restore"){
                require "backuprestore.php";
            }elseif($_GET['page'] == "keluar"){
                require "logout.php";
            }elseif($_GET['page'] == "rekap_pdf"){
                require "rekap_pdf.php";    
            }else{
                echo "<div id='rightContent'><h2 style='color:red;'>Page not found !!</h2></div>";
            }
        }

    ?>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <p><!-- BEGIN FOOTER --></p>

                <!-- END FOOTER -->
            </div>
        </div>
        <!-- END CONTAINER -->
         <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <!-- <script src="../assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script> -->
        <script src="../assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout6/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>
<!-- FOOTER -->
<div class="well well-sm" align="center"><a href=""><?= constant("NAMA_SEKOLAH") ?></a><span><b> Version 2.3.1</b></div> 
</html>
<?php } ?>
<?php 

ob_flush(); 

// 
// 
// *
// * *
// * * *
// Original App by Woka Inc | CBT Project  
// Modified version v1.0 by Aldi Fajrin | https://www.facebook.com/aldifa0512
// Big thanks to Woka Inc.
// *
// * * *
// * *
// *
// 
// 


?>