 <?php
    $msg = '';
    if(isset($_POST['submit'])){
        // Validasi jumlah presentase nilai
        $p_essay = $_POST['p_essay'];
        $p_objektif = $_POST['p_objektif'];

        $p_total = $p_essay + $p_objektif;
        if($p_total !== 100){
            $msg = '<div class="msg" id="msg"><span class="msg-err">Total persentase nilai harus sama dengan 100</span></div>';
        }else{
            $pengaturan = array(
                'tampilkan_nilai' => $_POST['tampilkan_nilai'],
                'p_essay' => $_POST['p_essay'],
                'p_objektif' => $_POST['p_objektif']
            );

            $simpan = simpanPengaturan($pengaturan);
            if($simpan){
                $msg = '<div class="msg" id="msg">Berhasil menyimpan pengaturan aplikasi.</div>';
            }
        }
    }

    // MEmbaca pengaturan aplikasi
    function bacaPengaturan(){
        $file = fopen("pengaturan_aplikasi.txt", "r") or die("Tidak dapat membuka file!");
        $pengaturan = fread($file, filesize("pengaturan_aplikasi.txt"));
        
        if($pengaturan){
            fclose($file);
            return $pengaturan;
        }
        return FALSE;
    }

    // Menyimpan pengaturan aplikasi 
    function simpanPengaturan($pengaturan){
        $file = fopen("pengaturan_aplikasi.txt", "w") or die("Tidak dapat membuka file!");
        
        $pengaturan = array(
            'tampilkan_nilai' => $pengaturan['tampilkan_nilai'],
            'p_essay' => $pengaturan['p_essay'],
            'p_objektif' => $pengaturan['p_objektif']
        );

        if(fwrite($file, json_encode($pengaturan))){
            fclose($file);
            return TRUE;
        }
        return FALSE;
    }

    $pengaturan =  bacaPengaturan();
    echo '<script type="text/javascript">
            // Set Nilai setingan saat ini
            window.onload = function(){
                var index = "";
                var pengaturan = '. $pengaturan . ';
                console.log(pengaturan);
                if (pengaturan["tampilkan_nilai"] == "Ya") index = 0;
                else index = 1;
                document.getElementById("tampilkan_nilai").selectedIndex = index; 
                document.getElementById("p_essay").value = pengaturan["p_essay"]; 
                document.getElementById("p_objektif").value = pengaturan["p_objektif"]; 
                document.getElementById("dp_essay").innerHTML = pengaturan["p_essay"]; 
                document.getElementById("dp_objektif").innerHTML = pengaturan["p_objektif"]; 
            } 
        </script>'
?> 
<div class="wrapper">
    <div class="form">
        <div><?= $msg ?></div>
        <form action="" method="POST">
            <fieldset>
                <legend>Pengaturan Display Nilai</legend>
                <table>
                    <tr>
                        <td class="form-lable">Tampilkan Nilai </td>
                        <td>: 
                            <select name="tampilkan_nilai" id="tampilkan_nilai">
                                <option>Ya</option>  
                                <option>Tidak</option>  
                            </select>
                        </td>
                    </tr>
                </table>                
              </fieldset>
              <div class="divider"></div>
              <legend>Perhitungan Nilai</legend>
                <table>
                    <tr>
                        <td class="form-lable">Presentase Essay </td>
                        <td>: 
                            <input type="number" name="p_essay" id="p_essay" />%
                        </td>
                    </tr>
                    <tr>
                        <td class="form-lable">Presentase Objektif </td>
                        <td>: 
                            <input type="number" name="p_objektif" id="p_objektif" />%
                        </td>
                    </tr>
                </table><br>
                <pre>
Perhitungan saat ini :

Jumlah betul Essay * <span id="dp_essay"></span>% = ... nilai
Jumlah betul Objektif * <span id="dp_objektif"></span>% = ... nilai
                </pre>              
              </fieldset>
              <input type="submit" name="submit" value="simpan" class="submit">
        </form>
    </div>
</div>
<style type="text/css">
    .form-lable{
        width: 150px;
    }
    .divider{
        height: 50px;
    }
    .submit{
        float: left;
        margin: 15px;
        clear: both;
    }
    .msg{
        color: #4286f4;
        font-size: 16pt;
        padding: 7px;
        background-color: #dce3ef;
    }   
    .msg-err{
        color: red;
    }
</style>