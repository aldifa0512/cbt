<?php

// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------
// iINCLUDE VENDOR DAN CONSTANT PADA ROOT DIRECTORY
//

    require_once('../constant.php');

//
// ---------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------


$date = date('Y');
//koneksi database
mysql_connect(constant("HOST"), constant("DATABASE_USER"), constant("DATABASE_PASSWORD"));
mysql_select_db(constant("DATABASE"));//fungsi pagination

$query = mysql_query("SELECT * FROM cbt_siswa");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Print Kartu Ujian Siswa</title>
<style type="text/css">
  table{ border-collapse: collapse;}
  .container{ display: inline-block; }
  .card{ width: 9cm; border: 1px solid black;border-collapse:collapse;margin: 0.10cm;position: relative;float: left;max-height: 350px;}
  .header-card td{text-align: center; border-bottom: 1px solid black;height: 2cm;}
	.pagebreak{ page-break-before: always; }
  .content-table tr td{ overflow: hidden; white-space: nowrap; max-width: 5.5cm;}
  .content-text{ font-size: 10pt; }
  .ct-label{ padding-right: 0.2cm; overflow: hidden;}
  .ct-value{ padding-left: 0.2cm; }
	.header-text{ font-size: 10pt; position: relative;top: auto;left: auto;}
	.header-logo{ max-width: 1.50cm; position:absolute;top: 0.3cm;left: 0.3cm;float: left;overflow: hidden;}
  .content-photo-card{ height: 3cm;width: 2cm;border: 1px solid black;margin-left: 0.1cm;margin-bottom: 0.1cm; display:flex;align-items: center;justify-content:center;}
  .content-footer-text{ vertical-align: middle; padding-top: 1cm; font-size: 10pt;}
</style>
</head>
<body>
  <table class="container">
    <tr>
    <?php $row = 1; ?>
    <?php $number = 0; ?>
    <?php while ($record = mysql_fetch_array($query)) { ?>
    <?php $number++; ?>
    <?php if(!($number%2 == 0)) { echo "<tr>";  } ?>
        <td>
      	<table class="card">
        	<tr>
            <td>
              <table style="width:100%;">
                <tr class="header-card">
                  <td colspan="2">
                    <img class="header-logo" src="images/1.jpg">
                    <span class="header-text">KARTU PESERTA UJIAN USBN</span><br>
                    <span class="header-text"><?= $date . '/' . ($date+1); ?></span>
                  </td>
                </tr>
                <tr class="content-card">
                  <td colspan="2">
                    <table class="content-table">
                      <tr>
                          <td><span class="content-text ct-label">Nama Peserta</span></td>
                          <td>: <span class="content-text ct-value"> <?= $record['XNamaSiswa'] ?></span></td>
                      </tr>
                      <tr>
                        <td><span class="content-text ct-label">Program Keahlian</span></td>
                        <td>: <span class="content-text ct-value"> <?= $record['XKodeKelas'] ?></span></td>
                      </tr>
                      <tr>
                        <td><span class="content-text ct-label">Username</span></td>
                        <td>: <span class="content-text ct-value"> <?= $record['XNomerUjian'] ?></span></td>
                      </tr>
                      <tr>
                        <td><span class="content-text ct-label">Password</span></td>
                        <td>: <span class="content-text ct-value"> <?= $record['XPassword'] ?></span></td>
                      </tr>
                      <tr>
                        <td><span class="content-text ct-label">ID Server / Ruang</span></td>
                        <td>: <span class="content-text ct-value"> -</span></td>
                      </tr>
                      <tr>
                        <td><span class="content-text ct-label">ID Server Cadangan</span></td>
                        <td>: <span class="content-text ct-value"> -</span></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr class="content-footer">
                  <td><div class="content-photo-card"><span class="middle-center-text">Foto 2x3</span></div></td>
                  <td class="content-footer-text"><span class="">Panitia Ujian USBN</span></td>
                </tr>
              </table>
            </td>
        	</tr>
      	</table>
        </td>
    <?php if(($number%2 == 0)) { echo "</tr>"; $page++;  } ?>
    <?php if((page%3 == 0)) { echo "</table><table class='container'><tr>"; $page++; } ?>
    <?php } ?>
  </tr>
</table>
</body>
</html>
