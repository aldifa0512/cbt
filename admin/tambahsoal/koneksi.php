<?php

$host = constant("HOST");

$username = constant("DATABASE_USER");

$password = constant("DATABASE_PASSWORD");

$db_name = constant("DATABASE");

$mysqli = new mysqli($host, $username, $password, $db_name);

if(mysqli_connect_errno()) {

echo "Error: Could not connect to database.";

exit;

}

?>