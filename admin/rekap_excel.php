<?php
error_reporting(0);
require_once 'plugins/excel/PHPExcel.php';
include "cbt_con.php"; 

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//$var_soal = "$_REQUEST[ujian]";

$hasil = mysql_query("SELECT *,u.XStatusUjian as ujsta, u.XKodeMapel as kode_mapel
FROM cbt_siswa s
LEFT JOIN `cbt_siswa_ujian` u ON u.XNomerUjian = s.XNomerUjian
LEFT JOIN cbt_ujian c ON (u.XKodeSoal = c.XKodeSoal and u.XTokenUjian = c.XTokenUjian)
WHERE c.XStatusUjian = '1'");

// Set properties
$objPHPExcel->getProperties()->setCreator("Sigit Hariono")
      ->setLastModifiedBy("Sigit Hariono")
      ->setTitle("Office 2007 XLSX Test Document")
      ->setSubject("Office 2007 XLSX Test Document")
       ->setDescription("Salary Report")
       ->setKeywords("office 2007 openxml php")
       ->setCategory("Rekap Hasil Tes :");
 
// Add some data
$objPHPExcel->setActiveSheetIndex(0)
       	->setCellValue('A1', 'NO')
       	->setCellValue('B1', 'NO. PESERTA')
       	->setCellValue('C1', 'NAMA SISWA')
       	->setCellValue('D1', 'KELAS')
       	->setCellValue('E1', 'NIK')
		->setCellValue('F1', 'NAMA MAPEL')
       	->setCellValue('G1', 'JAWAB')
       	->setCellValue('H1', 'BENAR')
		->setCellValue('I1', 'SALAH')
       	->setCellValue('J1', 'JUMLAH OBJ')
       	->setCellValue('K1', 'JUMLAH ESS')
       	->setCellValue('L1', 'BETUL OBJ')
       	->setCellValue('M1', 'BETUL ESS')
       	->setCellValue('N1', 'NILAI'); 

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);


$objPHPExcel->getActiveSheet()->getStyle("A1:N1")->getFont()->setBold( true );

$baris = 2;
$no = 0;		
$kode_mapel = "Rekap Nilai - Result Not Found";

while($p = mysql_fetch_array($hasil)){
	$kode_mapel = "Rekap Nilai - " . "$p[kode_mapel]";
	$token = $p[XTokenUjian];
    $var_siswa = "$p[XNomerUjian]";
	$var_token = "$p[XTokenUjian]";
	$var_soal = "$p[XKodeSoal]";

	$sqlujian = mysql_query("SELECT * FROM `cbt_jawaban` j left join cbt_soal s on s.XNomerSoal = j.XNomerSoal WHERE j.XKodeSoal = '$var_soal' and j.XUserJawab = '$var_siswa'
	and XTokenUjian = '$var_token'");
	
	$sqlmapel = mysql_query("select * from cbt_ujian c left join cbt_mapel m on m.XKodeMapel = c.XKodeMapel where c.XKodeSoal = '$var_soal'"); 
	$u = mysql_fetch_array($sqlmapel);
	$namamapel = $u['XNamaMapel'];
	
	$sqlsiswa = mysql_query("SELECT * FROM `cbt_siswa` WHERE XNomerUjian= '$var_siswa'");
	$s = mysql_fetch_array($sqlsiswa);
	$namsis = $s['XNamaSiswa'];
	$nk = $s['XNamaKelas'];
	$kk = $s['XKodeKelas'];
	$namkel = $nk.$kk;
	$nomsis = $s['XNIK'];

//----- php excel 

$no = $no +1;

	$sqldijawab = mysql_num_rows(mysql_query(" SELECT * FROM `cbt_jawaban` WHERE XKodeSoal = '$var_soal' and XUserJawab = '$var_siswa' and (XJawaban != '' || XJawabanEssay != '') and XTokenUjian = '$var_token'"));

	$queryJumlahBenarObjektif = "SELECT s.XEssay, j.XNilai as HasilUjian FROM `cbt_jawaban` j left join cbt_soal s on s.XNomerSoal = j.XNomerSoal WHERE j.XKodeSoal = '$var_soal' and  s.XKodeSoal = '$var_soal' and  j.XUserJawab = '$var_siswa' and j.XTokenUjian = '$var_token' order by j.Urut";
	$sqo = mysql_query($queryJumlahBenarObjektif);

	$queryJumlahBenarEssay = "SELECT count( j.XNilai ) as HasilUjian FROM `cbt_jawaban` j left join cbt_soal s on s.XNomerSoal = j.XNomerSoal WHERE j.XKodeSoal = '$var_soal' and  s.XKodeSoal = '$var_soal' and  j.XUserJawab = '$var_siswa' and j.XTokenUjian = '$var_token' and s.XEssay == '1' order by j.Urut";
	$sqe = mysql_fetch_array($queryJumlahBenarEssay);

	$jumlahSoalEssay = 0;
	$jumlahBetulEssay = 0;
	$jumlahSoalObjektif = 0;
	$jumlahBetulObjektif = 0;
	while($p = mysql_fetch_array($sqo)) {
		//Menghitung betul Essay
		if($p['XEssay'] == 1){
			$jumlahSoalEssay++;
			if($p['HasilUjian'] == 1){
				$jumlahBetulEssay++;
			}
		}else{
			// yang ini untuk betul essay
			$jumlahSoalObjektif++;
			if($p['HasilUjian'] == 1){
				$jumlahBetulObjektif++;
			}
		}	
	}

	$jumbenar =  intval($jumlahBetulEssay) + intval($jumlahBetulObjektif);

	$sqlsalah = mysql_query(" SELECT count( XNilai ) AS HasilUjian FROM `cbt_jawaban` WHERE XKodeSoal = '$var_soal' and XUserJawab = '$var_siswa' and XNilai = '0' and XTokenUjian = '$var_token'");
	$sqs = mysql_fetch_array($sqlsalah);
	$jumsalah = $sqs['HasilUjian'];
	
	$sqljumsoal = mysql_query(" SELECT count( XNilai ) AS HasilUjian FROM `cbt_jawaban` WHERE XKodeSoal = '$var_soal' and XUserJawab = '$var_siswa' and XTokenUjian = '$var_token'");
	$sqjsoal = mysql_fetch_array($sqljumsoal);
	$jumsoal = $sqjsoal['HasilUjian'];

	// -------------------------- PENILAIAN-----------------------------------//
	$persen_objektif = constant("PERSEN_OBJEKTIF");
	$persen_essay = constant("PERSEN_ESSAY");

	$nilaiEssay = intval($persen_essay) * (intval($jumlahBetulEssay) / intval($jumlahSoalEssay));
	$nilaiObjektif = intval($persen_objektif) * (intval($jumlahBetulObjektif) / intval($jumlahSoalObjektif));
	$totalnilai = $nilaiEssay + $nilaiObjektif;

$objPHPExcel->setActiveSheetIndex(0)
     	->setCellValue("A$baris", $no)
     	->setCellValue("B$baris", $p['XNomerUjian'])
     	->setCellValue("C$baris", $namsis)
     	->setCellValue("D$baris", $namkel)
     	->setCellValue("E$baris", $nomsis)
		->setCellValue("F$baris", $namamapel)
     	->setCellValue("G$baris", $sqldijawab)
     	->setCellValue("H$baris", $jumbenar)
		->setCellValue("I$baris", $jumsalah)
     	->setCellValue("J$baris", $jumlahSoalObjektif)
     	->setCellValue("K$baris", $jumlahSoalEssay)				
     	->setCellValue("L$baris", $jumlahBetulObjektif)			
     	->setCellValue("M$baris", $jumlahBetulEssay)			
     	->setCellValue("N$baris", $totalnilai);		

$baris = $baris + 1;
}

$styleArray = array(
  'borders' => array(
    'allborders' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getActiveSheet()->getStyle('A1:N'. ((int)$baris - 1))->applyFromArray($styleArray);

$objPHPExcel->setActiveSheetIndex(0)
     	->setCellValue("A" . ((int)$baris + 1) , $kode_mapel . ' -- Token : ' . $token);
$objPHPExcel->getActiveSheet()->getStyle("A" . ((int)$baris + 1))->getFont()->setBold( true );

$objPHPExcel->setActiveSheetIndex(0)
     	->setCellValue("A" . ((int)$baris + 2) , "Diproses langsung dari aplikasi " . constant("APP_NAME") . ' ' . constant("NAMA_SEKOLAH") . ' -- ' . date("Y-m-d H:i:s"));

$objPHPExcel->setActiveSheetIndex(0)
     	->setCellValue("A" . ((int)$baris + 3) , "TOTAL NILAI = (" . $persen_objektif . '% BETUL OBJEKTIF) + (' . $persen_essay . '% BETUL ESSAY)');

 
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('Hasil Ujian');
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client�s web browser (Excel5)
// header('Content-Type: application/vnd.ms-excel');
// header('Content-Disposition: attachment;filename=' . $kode_mapel . '.xls');
// header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$nama_file = str_replace(' ', '', $kode_mapel . '.xls');
$fileLocation = 'Laporan/' . $nama_file;
$objWriter->save($fileLocation);

header('Location: index.php?page=hasil_tes&download=true&file=' . $nama_file);
exit;
?>