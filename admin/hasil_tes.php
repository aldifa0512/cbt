<?php

require_once('../constant.php');
require_once("../cbt_con.php");
require_once('../vendor/session.php');
require_once('../vendor/FlashMessages.php');

require_once('databasesFunction.php');

function uploadLaporan(){
   $curl = curl_init();

   curl_setopt_array($curl, array(
      CURLOPT_URL => "http://cbt.smkn2pyk.sch.id/admin/api/laporan.php",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "username=aldi&password=secret&scope=user&name=aldi&email=aldi%40mail.com",
      CURLOPT_HTTPHEADER => array(
         "Cache-Control: no-cache"
      ),
   ));

   $response = curl_exec($curl);
   $err = curl_error($curl);

   curl_close($curl);

   if ($err) {
      echo "cURL Error #:" . $err;
   } else {
      echo $response;
   }
}
uploadLaporan();
$download = false;

if($_REQUEST['download'] == 'true'){
   $admin_path = dirname(__FILE__);

   $nama_file = str_replace(' ', '', $_REQUEST['file']);
   $fileLocation = $admin_path . '\\Laporan\\' . $nama_file;
   $zipFileName = $nama_file . '.7z';
   $password = constant("PASS_7Z");
   $stringExec = '7z a ' . $admin_path . '\\Laporan\\' . str_replace(' ', '%%',$zipFileName) . ' ' . str_replace(' ', '%%',$fileLocation) . ' -p' . $password;
   $exec = shell_exec($stringExec);
   //var_dump(dirname(__FILE__));
   //var_dump($stringExec);
   //var_dump($exec);//die();
   if($exec){
      if (file_exists($fileLocation)) {
         unlink($fileLocation);
      }

      $download = true;
   }else{
      if (file_exists($fileLocation)) {
         unlink($fileLocation);
      }
      echo "Terjadi Gangguan Saat Menulis File.";
   }
}
?>
  <div class="tab-pane" id="hasil-test-new">
  <style>
a.ex1:link, a.ex1:visited {
    background-color: #25a344;
    color: white;
    padding: 14px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
}


a.ex1:hover, a.ex1:active {
    background-color: #1f8e3b;
}

a.ex2:link, a.ex2:visited {
    background-color: #f44336;
    color: white;
    padding: 14px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
}


a.ex2:hover, a.ex2:active {
    background-color: red;
}
</style>                               
<? 
$sqltes1= mysql_query("select * from cbt_ujian where XTokenUjian = 'dsd' ");
$sko = mysql_fetch_array($sqltes1);
$koso = $sko['XKodeSoal'];
?>
                <div class="alert alert-info" style="clear: both;">
                <b><span style="font-weight: bold;color: red;">Pastikan hanya ada satu tes yang aktif</span> agar laporan tidak overlap satu sama lain.</b></div>
                <?php if($download == true) { ?>
                <div class="alert alert-info" style="clear: both;">
                <b><a style="cursor:pointer" href='../admin/download_backup_data.php?laporan=true&nama_file=<?php echo str_replace(' ', '%%',$zipFileName); ?>'" title="Download">Download Laporan Disini !</a></b></div>
                <?php } ?> 
                 <br><a class="ex1" href="rekap_excel.php?ujian=<? echo $koso; ?>"><i class="icon-notebook"></i> Download File Excel Rekap</a><span>
                 <a class="ex2" href="rekap_excel.php?ujian=<? echo $koso; ?>"><i class="icon-notebook"></i> Upload Laporan </a><span>
                 <!-- <a class="ex2" href="rekap_pdf.php?ujian=<? echo $koso; ?>"><i class="icon-doc"></i> Download File PDF</a> -->
<script type="text/javascript">
var auto_refresh = setInterval(
   function ()
   {

      $('#load_hasil').load('hasilpeserta.php').fadeIn();
                                }, 1000); // refresh every 10000 milliseconds


                                </script>

                <div  class="load_hasil" id="load_hasil">  
                </div>

                 </div>

                <div  class="load_comment" id="load_comment">  
                </div>

                 </div>
